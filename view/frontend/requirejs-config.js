/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map : {
        '*': {
            "mau/resumable"    : 'Mau_Core/js/lib/resumable',
            "mau/ko"           : 'Mau_Core/js/lib/ko-bindings',
            "mau/icons"        : 'Mau_Core/js/icons',
            "mau/utils"        : 'Mau_Core/js/utils',
            "mau/validation"   : 'Mau_Core/js/lib/validation',
            "mau/catalog"      : 'Mau_Core/js/freedom-catalog',
            "mau/form"         : 'Mau_Core/js/freedom-form',
            "mau/list"         : 'Mau_Core/js/freedom-list',
            "mau/action/base"  : 'Mau_Core/js/actions/base',
            "mau/action/save"  : 'Mau_Core/js/actions/save',
            "mau/action/delete": 'Mau_Core/js/actions/delete',
            "mau/action/load"  : 'Mau_Core/js/actions/load',
            "mau/action/search": 'Mau_Core/js/actions/search',
            "mapping"              : 'Mau_Core/js/lib/ko-mapping',
            "jstree"               : 'Mau_Core/js/jquery/jstree',
            "mask"                 : 'Mau_Core/js/jquery/jquery.mask',
            "numeric"              : 'Mau_Core/js/jquery/jquery.numeric',
            "Croppie"              : 'Mau_Core/js/croppie/croppie',
            "Webcam"               : 'Mau_Core/js/webcam/webcam',
            "FullCalendar"         : 'Mau_Core/js/FullCalendar/main',
            "FullCalendarEs"       : 'Mau_Core/js/FullCalendar/locales/es',
            "autoComplete"         : 'Mau_Core/js/autoComplete/jquery.auto-complete'
        }
    },
    shim: {
        'autoComplete': {
            deps: [
                'jquery',
            ]
        },
        'mask'        : {
            deps: [
                'jquery',
            ]
        },
        'numeric'     : {
            deps: [
                'jquery',
            ]
        }
    },
};
