<?php

namespace Mau\Core\Api;

interface FileUploaderInterface {

    public function uploadChunk();

    public function generateId();

}
